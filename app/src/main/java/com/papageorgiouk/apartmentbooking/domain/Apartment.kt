package com.papageorgiouk.apartmentbooking.domain

import android.os.Parcelable
import com.prolificinteractive.materialcalendarview.CalendarDay
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Apartment(val id: Int, val beds: Int, val datesBooked: MutableList<CalendarDay> = mutableListOf()) : Parcelable {

    override fun equals(other: Any?): Boolean {
        return other is Apartment && other.id == this.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}