import com.papageorgiouk.apartmentbooking.domain.Apartment
import com.prolificinteractive.materialcalendarview.CalendarDay

/**
 * The apartment manager will manage everything apartment-related
 */
object ApartmentManager {

    var apartments: MutableList<Apartment> = mutableListOf()

    init {
        //  add 4 1-bedrooms
        for (i in 1..4) { apartments.add(Apartment(100 + i, 1)) }

        //  add 3 2-bedrooms
        for (i in 1..3) { apartments.add(Apartment(200 + i, 2)) }

        //  add 3 3-bedrooms
        for (i in 1..3) { apartments.add(Apartment(300 + i, 3)) }

    }

    /**
     * Check if apartments are available for the selected dates and return them.
     * If not, return null
     */
    fun getAvailableApts(beds: Int, datesInterested: List<CalendarDay>): List<Apartment>? {

        //  filter apartments with the desired number of beds
        val suitableApts = apartments.filter { it.beds == beds }

        //  filter only apartments for which their booked dates do not intersect with
        //  dates the user is interested in. That means they're available
        return suitableApts.filter {
            it.datesBooked.intersect(datesInterested).isEmpty()
        }

    }

    //  Add newly-booked days to the apartment's booked days
    fun bookApartment(apartment: Apartment, dates: List<CalendarDay>) {
        apartments.filter { it == apartment }
                .first()  //  it's just one
                .let { it.datesBooked.addAll(dates) }
    }

}