package com.papageorgiouk.apartmentbooking.ui

import ApartmentManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.papageorgiouk.apartmentbooking.R
import com.papageorgiouk.apartmentbooking.domain.Apartment
import com.prolificinteractive.materialcalendarview.CalendarDay
import kotlinx.android.synthetic.main.activity_book.*

class BookActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book)

        val dates = intent.getParcelableArrayExtra("dates") as List<CalendarDay>

        var apartments: MutableList<Apartment> = mutableListOf()
        if (intent.hasExtra("apartments")) apartments = intent.getParcelableArrayListExtra("apartments")

        if (apartments.isNotEmpty()) {
            txt_found.text = "We found ${apartments.size} apartment(s)"
            btn_book.visibility = View.VISIBLE
        } else {
            txt_found.text = "No available apartments"
            btn_book.visibility = View.GONE
        }

        btn_book.setOnClickListener {
            ApartmentManager.bookApartment(apartments.first(), dates)
        }
    }
}
