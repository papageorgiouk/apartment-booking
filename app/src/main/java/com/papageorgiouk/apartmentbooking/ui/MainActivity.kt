package com.papageorgiouk.apartmentbooking.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.papageorgiouk.apartmentbooking.R
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //  set the minimum number of beds to 1 and the maximum to 4
        np_beds.apply {
            minValue = 1
            maxValue = 4
            value = 1  //  initial value
        }

        //  set the calendar's selection mode to "range", for selecting a range of dates
        //  I actually wrote this feature myself :D
        //  https://github.com/prolificinteractive/material-calendarview/pull/314
        calendar.selectionMode = MaterialCalendarView.SELECTION_MODE_RANGE


        var selectedDays: MutableList<com.prolificinteractive.materialcalendarview.CalendarDay> = mutableListOf()
        calendar.setOnRangeSelectedListener { view, days ->
            selectedDays = days
        }

        btn_next.setOnClickListener {

            val apts = ApartmentManager.getAvailableApts(np_beds.value, selectedDays)
            val intent = Intent(this, BookActivity::class.java)
            apts?.let { intent.putExtra("apartments", ArrayList(it)) }
            intent.putExtra("dates", ArrayList(selectedDays))
            startActivity(intent)
        }

    }
}
